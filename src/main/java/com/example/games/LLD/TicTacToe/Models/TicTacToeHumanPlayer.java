package com.example.games.LLD.TicTacToe.Models;

import com.example.games.LLD.TicTacToe.Constants.Enums;

public class TicTacToeHumanPlayer extends TicTacToePlayer {
    public TicTacToeHumanPlayer(final String id, final String name, final Enums.TicTacToeCharacters character) {
        super(id, name, character);
    }
}
