package com.example.games.LLD.TicTacToe.Constants;

public final class TicTacToeAttributes {
    public static final String BOARD = "board";
    public static final String X = "x";
    public static final String Y = "y";
    public static final String DEFAULT_CHARACTER = "defaultCharacter";
    public static final String PLAYER_MANAGER = "playerManager";
    public static final String DEFAULT_PLAYER_1 = "Player 1";
    public static final String DEFAULT_PLAYER_2 = "Player 2";
}
