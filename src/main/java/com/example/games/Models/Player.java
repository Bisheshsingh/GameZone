package com.example.games.Models;

import lombok.Data;

@Data
public abstract class Player {
    private final String id;
    private final String name;
}
