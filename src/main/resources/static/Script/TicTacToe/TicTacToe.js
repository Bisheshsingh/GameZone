var redirectToPage = function(link) {
    window.location.href = link;
}

var disable = function(element, disable) {
    element.disabled = disable;
}